#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
@File   :   tools.py
@Time   :   2021/12/14 14:27
@Author :   Blank
@Version:   1.0
@Desc   :   常用工具集
'''

import msvcrt


def input_password(info: str = 'password: ') -> tuple:
    """shell密码输入

    按键绑定: 回车确认, 退格删除, Esc退出(返回False)

    :param info: 提示信息, defaults to 'password: '
    :type info: _type_, str
    :return: 信号和密码
    :rtype: tuple
    """
    print(info, end='', flush=True)
    pawd_arr = []
    while True:

        keypad = msvcrt.getch()
        # 回车
        if keypad == b'\r':
            msvcrt.putch(b'\n')
            return True, b''.join(pawd_arr).decode()
        # 退格
        elif keypad == b'\x08':
            if pawd_arr:
                pawd_arr.pop()
                msvcrt.putch(b'\b')
                msvcrt.putch(b' ')
                msvcrt.putch(b'\b')
        # Esc
        elif keypad == b'\x1b':
            msvcrt.putch(b'\n')
            return False, ''
        else:
            pawd_arr.append(keypad)
            msvcrt.putch(b'*')
