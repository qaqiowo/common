#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
@File   :   network.py
@Time   :   2021/12/21 11:55
@Author :   Blank
@Version:   1.0
@Desc   :   网络相关工具
'''

import socket


def get_host_ip() -> str:
    """ 获取主机IP """
    return socket.gethostbyname(socket.gethostname())


def check_port_open(host: str = 'localhost', port: int = 80) -> bool:
    """ 检测端口是否开放 """
    with socket.socket() as sock:
        sock.settimeout(2)
        try:
            sock.connect((host, port))
            return True
        except socket.timeout:
            return False
