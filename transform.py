import json
import os
import time
from typing import List

import pandas as pd
from coord_convert.transform import (bd2gcj, bd2wgs, gcj2bd, gcj2wgs, wgs2bd,
                                     wgs2gcj)


def json2csv(data: List[dict], filename: str = 'data', encoding='utf_8'):
    """ json数据转存到csv

    :param data: JSON数据
    :param filename: CSV文件名
    """

    filename = filename if filename[-4:] == '.csv' else f'{filename}.csv'

    format_data = [{k: json.dumps(v, ensure_ascii=False).replace(',', ' ').replace('\n', ' ') for k, v in item.items()} for item in data]
    dataframe = pd.DataFrame(format_data)

    # 将DataFrame存储为csv,index表示是否显示行名，default=True
    if os.path.exists(filename):
        dataframe.to_csv(filename, mode='a', encoding=encoding, index=False, index_label=False, sep=',', header=False)
    else:
        dataframe.to_csv(filename, mode='a', encoding=encoding, index=False, index_label=False, sep=',')


def csv_to_xlsx(csv_dir):
    csv = pd.read_csv(csv_dir, encoding='utf-8')
    csv.to_excel(f'{csv_dir[:-4]}.xlsx', sheet_name='data')

 # ------------------------------------ dict list str ------------------------------------ #


def dict2list(dict_obj: dict) -> list:
    """ {'a': 2, 'b': 3} -> ['a=2', 'b=3'] """
    return [f'{k}={v}' for k, v in dict_obj.items()]


def list2dict(list_obj: list) -> dict:
    """ ['a', 1, 'b', 2] -> {'a': 1, 'b': 2} """
    return dict(zip(list_obj[::2], list_obj[1::2]))


def dict2cookies(dict_obj: dict) -> str:
    """ {'locale': 'zh-cn', 'host': 'google.com'} -> 'locale=en; host=google.com' """
    return "; ".join(dict2list(dict_obj))


def cookies2dict(cookies: str) -> dict:
    """ 'locale=en; host=google.com' -> {'locale': 'zh-cn', 'host': 'google.com'} """
    cookies = [cookie.strip().split("=") for cookie in cookies.split(";")]
    return dict(cookies)


def urlparam2dict(param: str) -> dict:
    """ 'token=1024&url=http%3A%2F%2F' -> {'token': '1024', 'url': ':http%3A%2F%2F'} """
    return dict(p.split('=') for p in param.split('&'))


# ------------------------------------ Time ------------------------------------ #

def unixtime2str(time_secs: int, time_zone: int = 8, time_format='%Y-%m-%d %H:%M:%S'):
    """
    Convert Unix time (UTC seconds) to time string with given time zone.

    @param time_secs: UTC seconds.
    @param time_zone: the time zone of returned time, the range [-12, 12].
    @param time_format: the time format of returned time.
    @return: time string
    """
    time_secs += time_zone * 3600
    return time.strftime(time_format, time.gmtime(time_secs))


def timestamp2datetime(value: int):
    """ 1620481024 -> '2021-05-08 21:37:04' """
    struct_time = time.localtime(value)
    return time.strftime('%Y-%m-%d %H:%M:%S', struct_time)


def timestamp2utcdatetime(value):
    """ '2021-05-08 21:37:04' -> 1620481024 """
    struct_time = time.gmtime(value)
    return time.strftime('%Y-%m-%d %H:%M:%S', struct_time)


# ------------------------------------ list ------------------------------------ #

def list_partition(list_obj: list, cnt: int):
    """ 分割list为小list

    :param cnt 数量
    :return [] -> [[],[]]
    """
    return [list_obj[i:i + cnt] for i in range(0, len(list_obj), cnt)]


# ------------------------------------ geo ------------------------------------ #

def geo2all(type:str, lon:float, lat:float) -> dict:
    """地理坐标系转换, 保留6位小数

    Args:
        type (str): 传入坐标系类型
            - WGS84: 为一种大地坐标系,也是目前广泛使用的GPS全球卫星定位系统使用的坐标系。
            - GCJ02: 又称火星坐标系,是由中国国家测绘局制定的地理坐标系统,是由WGS84加密后得到的坐标系。
            - BD09: 为百度坐标系,在GCJ02坐标系基础上再次加密。
        lon (float): 径度
        lat (float): 纬度
    """

    if type.upper() == 'WGS84':
        wgslon, wgslat = lon, lat
        gcjlon,gcjlat = wgs2gcj(lon, lat)
        bdlon,bdlat = wgs2bd(lon, lat)
    elif type.upper() == 'GCJ02':
        wgslon, wgslat = gcj2wgs(lon, lat)
        gcjlon,gcjlat = lon, lat
        bdlon,bdlat = gcj2bd(lon, lat)
    elif type.upper() == 'BD09':
        wgslon, wgslat = bd2wgs(lon, lat)
        gcjlon,gcjlat = bd2gcj(lon, lat)
        bdlon,bdlat = lon, lat
    else:
        raise TypeError

    geo = {
        'geoPointWGS84': {'lon':float(f'{wgslon:.6f}'), 'lat':float(f'{wgslat:.6f}')},
        'geoPointGCJ02': {'lon':float(f'{gcjlon:.6f}'), 'lat':float(f'{gcjlat:.6f}')},
        'geoPointBD09': {'lon':float(f'{bdlon:.6f}'), 'lat':float(f'{bdlat:.6f}')},
    }
    return geo
