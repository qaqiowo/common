#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
@File   :   funs.py
@Time   :   2021/10/18 12:27
@Author :   Blank
@Version:   1.0
@Desc   :   常用函数库
'''

import difflib
import inspect
import re
import time


def re_search_one(pattern, string) -> str:
    """ 返回第一个正则匹配的元素 """
    if result := re.search(pattern, string):
        return result.group(1)
    return None


def wait_time(sec: int):
    """ 等待时间输出 """
    cnt = 0
    for i in range(sec + 1):
        print(f'\r{" "*6}\r{sec-i}{"."*(cnt%3+1)}', end='')
        cnt += 1
        time.sleep(1)
    print(f'\r{" "*6}\r', end='')


def string_similar(str_1, str_2):
    """ 字符串相似度计算 """
    return difflib.SequenceMatcher(None, str_1, str_2).quick_ratio()

def get_funs_name():
    """ 获取当前运行函数名 """
    return inspect.stack()[1][3]

def out_of_china(lon, lat):
    """ 中国经纬度判断 """
    if lon < 72.004 or lon > 137.8347:
        return True
    if lat < 0.8293 or lat > 55.8271:
        return True
    return False
